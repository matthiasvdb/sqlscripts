USE ModernWays;
INSERT INTO boeken (
familienaam,
titel,
verschijningsjaar,
categorie )
VALUES ('?', 'beowulf', '0975', 'Mythologie')
,
('Ovidius', 'Metamorfosen', '8', 'Mythologie');

SELECT titel FROM boeken
WHERE verschijningsjaar < 0967;
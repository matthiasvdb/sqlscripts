USE ModernWays;
ALTER TABLE huisdieren ADD COLUMN geluid VARCHAR(20) CHAR SET utf8mb4;
UPDATE huisdieren
SET geluid = 'WAF!'
WHERE soort = 'hond';

USE ModernWays;
UPDATE huisdieren
SET geluid = 'miauwww...'
WHERE soort = 'kat';
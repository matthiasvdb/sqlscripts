USE ModernWays;
CREATE TABLE Metingen(
Tijdstip DATETIME NOT NULL,
Grootte INT(5) unsigned NOT NULL,
Marge double(3,2) NOT NULL
);